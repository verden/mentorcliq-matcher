package com.mentorcliq.common.exception;

/**
 * @author William Arustamyan
 */

public class InvalidFileException extends RuntimeException {

    public InvalidFileException(final Throwable exc) {
        super("invalid CSV file ... ", exc);
    }

}
