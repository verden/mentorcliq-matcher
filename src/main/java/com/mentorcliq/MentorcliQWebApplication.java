package com.mentorcliq;

import com.mentorcliq.resources.EmployeeResource;
import com.mentorcliq.server.JettyServer;
import com.mentorcliq.server.RestServer;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 * @author William Arustamyan
 */


public class MentorcliQWebApplication {


    public static void main(String[] args) {
        final Class<?>[] resources = {EmployeeResource.class, MultiPartFeature.class};
        final RestServer server = new JettyServer(8080, "/", resources);
        try {
            server.start();
        } catch (Exception e) {
            throw new IllegalStateException("Unable start Jetty server ...", e);
        }
    }
}
