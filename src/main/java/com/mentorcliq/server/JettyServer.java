package com.mentorcliq.server;

import com.mentorcliq.config.CORSFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * @author William Arustamyan
 */

public class JettyServer implements RestServer {

    private final Server jettyServer;

    public JettyServer(int port, String contextPath, Class<?>[] resources) {
        final ResourceConfig config = new ResourceConfig(resources);
        config.register(new CORSFilter());
        ServletHolder jerseyServlet = new ServletHolder(new ServletContainer(config));
        this.jettyServer = new Server(port);
        ServletContextHandler context = new ServletContextHandler(this.jettyServer, contextPath);
        context.addServlet(jerseyServlet, "/*");
    }

    @Override
    public void start() throws Exception {
        try {
            this.jettyServer.start();
            this.jettyServer.join();
        } finally {
            this.jettyServer.destroy();
        }
    }
}
