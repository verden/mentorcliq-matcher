package com.mentorcliq.server;

/**
 * @author William Arustamyan
 */

public interface RestServer {

    void start() throws Exception;

}
