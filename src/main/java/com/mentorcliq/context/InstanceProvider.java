package com.mentorcliq.context;

/**
 * @author William Arustamyan
 */

public interface InstanceProvider<T> {


    T provide();
}
