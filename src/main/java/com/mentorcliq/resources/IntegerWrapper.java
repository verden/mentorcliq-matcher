package com.mentorcliq.resources;

/**
 * @author William Arustamyan
 */

public class IntegerWrapper {


    private int value;
    public IntegerWrapper() {
    }

    public IntegerWrapper(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
