package com.mentorcliq.resources;

import com.mentorcliq.config.SessionFactoryProvider;
import com.mentorcliq.entity.Employee;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author William Arustamyan
 */


@Path("employees")
public class EmployeeResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/employee")
    public Response status() {
        Session session = SessionFactoryProvider.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(new Employee("empl", "www@gmail.com", "Accounting", 30, 2));
        transaction.commit();

        List<Employee> employees = session.createQuery("from Employee ", Employee.class).list();

        employees.forEach(System.out::println);
        session.close();
        return Response.status(Response.Status.OK).entity(new IntegerWrapper(200)).build();
    }

    @POST
    @Path("/employee/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response employeeFileUpload(@FormDataParam("file") InputStream stream,
                                       @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
        String line;
        while ((line = reader.readLine()) != null) {

            System.out.println(line);

        }

        return Response.status(Response.Status.OK).build();
    }
}
