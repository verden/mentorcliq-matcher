package com.mentorcliq.service.implementation;

import com.mentorcliq.common.exception.InvalidFileException;
import com.mentorcliq.entity.Employee;
import com.mentorcliq.service.GetSupported;
import com.mentorcliq.service.UploadSupported;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

public final class EmployeeService implements UploadSupported<List<Employee>>, GetSupported<Employee> {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);

    @Override
    public List<Employee> upload(final InputStream stream) throws InvalidFileException {
        final Reader reader = new InputStreamReader(stream);
        Iterable<CSVRecord> records;
        try {
            records = CSVFormat.EXCEL.parse(reader);
        } catch (IOException e) {
            logger.error("unable to parse csv file...", e);
            throw new InvalidFileException(e);
        }
        List<Employee> employees = this.convertToEmployees(records);
        //execute save
        return employees;
    }

    @Override
    public Employee get(final UUID uuid) {
        return null;
    }

    private List<Employee> convertToEmployees(final Iterable<CSVRecord> records) {
        final List<Employee> employees = new ArrayList<>();
        for (final CSVRecord r : records) {
            employees.add(new Employee(
                    r.get("Name"),
                    r.get("email"),
                    r.get("Division"),
                    Integer.parseInt(r.get("Age")),
                    Integer.parseInt(r.get("Timezone"))
            ));
        }
        return employees;
    }
}
