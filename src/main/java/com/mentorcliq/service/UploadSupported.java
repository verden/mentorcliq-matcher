package com.mentorcliq.service;

import java.io.InputStream;

/**
 * @author William Arustamyan
 */

public interface UploadSupported<T> {

    T upload(InputStream stream);
}
