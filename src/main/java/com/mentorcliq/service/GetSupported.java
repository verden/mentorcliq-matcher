package com.mentorcliq.service;

import java.util.UUID;

/**
 * @author William Arustamyan
 */

public interface GetSupported<T> {
    T get(UUID uuid);
}
